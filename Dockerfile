FROM ubuntu:latest
MAINTAINER Natalia Burakova "nata.burakova1997@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python3 python3-pip
COPY . /
WORKDIR /
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD [ "./start.py" ]


