class User:
    def __init__(self,  username, password = None, role = None):
        self.username = username
        self.password = password
        self.role = role

    def set_username(self, username):
        self.username = username

    def get_username(self):
        return self.username

    def set_password(self, password):
        self.password = password

    def get_password(self):
        return self.password

    def set_role(self, role):
        self.role = role

    def get_role(self):
        return self.role