class Deal:
    def __init__(self, instrumentName, cpty, price, type, quantity, time):
        self.instrumentName = instrumentName
        self.cpty = cpty
        self.price = price
        self.type = type
        self.quantity = quantity
        self.time = time

    def set_instrumentName(self, instrumentName):
        self.instrumentName = instrumentName

    def get_instrumentName(self):
        return self.instrumentName

    def set_cpty(self, cpty):
        self.cpty = cpty

    def get_cpty(self):
        return self.cpty

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price

    def set_type(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def set_quantity(self, quantity):
        self.quantity = quantity

    def get_quantity(self):
        return self.quantity

    def set_time(self, time):
        self.time = time

    def get_time(self):
        return self.time