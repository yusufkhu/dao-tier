from dbconnect.DBconnect import DBconnect


class CptyDao:
    __db = None

    def __init__(self):
        self.__db = DBconnect()

    def get_cptys(self):
        return self.__db.select("SELECT * FROM cptys", None).fetchall()

    def is_exist_cpty(self, cpty):
        sql_insert_query = """SELECT * FROM cptys WHERE counter_party = (%s)"""
        insert_tuple = (cpty,)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if (len(result) == 0):
            return False
        else:
            return True

    def save_cpty(self,  cpty):
        if not(self.is_exist_cpty(cpty)):
            sql_insert_query = """ INSERT INTO cptys(counter_party) VALUES (%s)"""
            insert_tuple = (cpty,)
            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_cpty_id(self, cpty):
        if (self.is_exist_cpty(cpty)):
            sql_select_query = """ SELECT id FROM cptys WHERE counter_party=(%s)"""
            select_tuple = (cpty,)
            return self.__db.select(sql_select_query, select_tuple).fetchall()[0][0]
        else:
            return -1