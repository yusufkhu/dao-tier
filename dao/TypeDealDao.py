from dbconnect.DBconnect import DBconnect


class TypeDealDao:
    __db = None

    def __init__(self):
        self.__db = DBconnect()

    def get_types_deal(self):
        return self.__db.select("SELECT * FROM types_deal", None).fetchall()

    def is_exist_type_deal(self, type_deal):
        sql_insert_query = """SELECT * FROM types_deal WHERE  type_deal = (%s)"""
        insert_tuple = (type_deal,)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if (len(result) == 0):
            return False
        else:
            return True

    def save_type_deal(self, type_deal):
        if (len(type_deal)==1 and not(self.is_exist_type_deal(type_deal))):
            sql_insert_query = """ INSERT INTO types_deal(type_deal) VALUES (%s)"""
            insert_tuple = (type_deal,)
            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_type_deal_id(self, type_deal):
        if (self.is_exist_type_deal(type_deal)):
            sql_select_query = """ SELECT id FROM types_deal WHERE type_deal=(%s)"""
            select_tuple = (type_deal,)
            result = self.__db.select(sql_select_query, select_tuple).fetchall()
            return result[0][0]
        else:
            return -1