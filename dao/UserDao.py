from dbconnect.DBconnect import DBconnect
from dao.RoleDao import RoleDao


class UserDao(object):
    __db = None

    def __init__(self):
        self.__db = DBconnect()
        self.__roleDao = RoleDao()

    def get_users(self):
        return self.__db.select("SELECT * FROM users", None).fetchall()

    def check_username_and_password_for_user(self, user):
        sql_insert_query = """SELECT * FROM users WHERE  user_name = (%s) and user_password = (%s) """
        insert_tuple = (user.get_username(), user.get_password(), )
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if len(result) == 0:
            return False
        else:
            return True

    def is_exist_user(self, user):
        sql_insert_query = """SELECT * FROM users WHERE  user_name = (%s)"""
        insert_tuple = (user.get_username(),)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if len(result) == 0:
            return False
        else:
            return True

    def save_user(self, user):
        if not(self.is_exist_user(user)):
            sql_insert_query = """ INSERT INTO users(user_name, user_password, user_role_id) VALUES (%s, %s, %s)"""
            role = user.get_role()
            if not(self.__roleDao.is_exist_role(role)):
                self.__roleDao.save_role(role)
            role_id = self.__roleDao.get_role_id(role)
            insert_tuple = (user.get_username(), user.get_password(), role_id,)
            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_user_id(self, user):
        if (self.is_exist_user(user)):
            sql_select_query = """ SELECT id FROM users WHERE user_name=(%s)"""
            select_tuple = (user.get_username(),)
            result = self.__db.select(sql_select_query, select_tuple).fetchall()
            return result[0][0]
        else:
            return -1

    def get_role_for_user(self, user):
        if (self.is_exist_user(user)):
            sql_select_query = """ SELECT user_role_id FROM users WHERE user_name=(%s)"""
            select_tuple = (user.get_username(),)
            result = self.__db.select(sql_select_query, select_tuple).fetchall()
            if len(result) == 0:
                return None
            return self.__roleDao.get_role(result[0][0])
        else:
            return -1




