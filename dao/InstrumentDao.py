from dbconnect.DBconnect import DBconnect

class InstrumentDao:
    __db = None

    def __init__(self):
        self.__db = DBconnect()

    def get_instruments(self):
        return self.__db.select("SELECT * FROM instruments", None).fetchall()


    def is_exist_instrument(self, instrument):
        sql_insert_query = """SELECT * FROM instruments WHERE  instrument_name = (%s)"""
        insert_tuple = (instrument,)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if (len(result) == 0):
            return False
        else:
            return True

    def save_instrument(self, instrument):
        if not(self.is_exist_instrument(instrument)):
            sql_insert_query = """ INSERT INTO instruments(instrument_name) VALUES (%s)"""
            insert_tuple = (instrument,)
            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_instrument_id(self, instrument):
        if (self.is_exist_instrument(instrument)):
            sql_select_query = """ SELECT id FROM instruments WHERE instrument_name =(%s)"""
            select_tuple = (instrument,)
            result = self.__db.select(sql_select_query, select_tuple).fetchall()
            return result[0][0]
        else:
            return -1

    def average_price(self, type_deal_id, instrument_id, time1, time2):

        sql_insert_query = """SELECT AVG(price) FROM deals 
                               WHERE type_deal_id = (%s) AND 
                               instrument_id = (%s) AND  time_deal>=(%s) AND time_deal <=(%s)"""

        insert_tuple = (type_deal_id, instrument_id, time1, time2)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        return result

    def average_price_without_date(self, type_deal_id, instrument_id):
        sql_insert_query = """SELECT AVG(price) FROM deals 
                               WHERE type_deal_id = (%s) AND 
                               instrument_id = (%s) """
        insert_tuple = (type_deal_id, instrument_id)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        return result