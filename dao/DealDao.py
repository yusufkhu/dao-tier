from dbconnect.DBconnect import DBconnect
from dao.CptyDao import CptyDao
from dao.InstrumentDao import InstrumentDao
from dao.TypeDealDao import TypeDealDao


class DealDao:
    __db = None

    def __init__(self):
        self.__db = DBconnect()
        self.__cptyDao = CptyDao()
        self.__typeDealDao = TypeDealDao()
        self.__instrumentDao = InstrumentDao()

    def get_historical_date(self, date_from, date_to):
        insert_tuple = (date_from, date_to)
        print(insert_tuple)
        return self.__db.select("SELECT * FROM deals WHERE time_deal>=(%s) AND time_deal <=(%s)",
                                insert_tuple).fetchall()

    def get_deals(self):
        return self.__db.select("SELECT * FROM deals", None).fetchall()

    def is_exist_deal(self, deal):
        cpty_id = self.__cptyDao.get_cpty_id(deal.get_cpty())
        instrument_id = self.__instrumentDao.get_instrument_id(deal.get_instrumentName())
        type_id = self.__typeDealDao.get_type_deal_id(deal.get_type())
        if (cpty_id != -1 and instrument_id!=-1 and type_id!=-1):
            sql_insert_query = """SELECT * FROM deals WHERE instrument_id=(%s) and counter_party_id=(%s) and type_deal_id=(%s) and time_deal=(%s) and quantity=(%s) and price=(%s)"""
            insert_tuple = (instrument_id, cpty_id, type_id, deal.get_time(), deal.get_quantity(), deal.get_price())
            result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
            if (len(result) == 0):
                return False
            else:
                return True
        else:
            return False

    def save_deal(self, deal):
        if not(self.is_exist_deal(deal)):
            sql_insert_query = """ INSERT INTO deals(instrument_id, counter_party_id, price, type_deal_id, quantity, time_deal) VALUES (%s, %s, %s, %s, %s, %s)"""

            type_id = self.get_type_id(deal)
            cpty_id = self.get_cpty_id(deal)
            instrument_id = self.get_instrument_id(deal)
            print(deal.get_time())
            insert_tuple = (instrument_id, cpty_id,deal.get_price(), type_id, deal.get_quantity(), deal.get_time(),)

            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_type_id(self, deal):
        type_deal = str(deal.get_type())
        if not(self.__typeDealDao.is_exist_type_deal(type_deal)):
            self.__typeDealDao.save_type_deal(type_deal)
        return self.__typeDealDao.get_type_deal_id(type_deal)

    def get_cpty_id(self, deal):
        cpty = str(deal.get_cpty())
        if not(self.__cptyDao.is_exist_cpty(cpty)):
            self.__cptyDao.save_cpty(cpty)
        return self.__cptyDao.get_cpty_id(cpty)

    def get_instrument_id(self, deal):
        instrument_name = str(deal.get_instrumentName())
        if not(self.__instrumentDao.is_exist_instrument(instrument_name)):
            self.__instrumentDao.save_instrument(instrument_name)
        return self.__instrumentDao.get_instrument_id(instrument_name)

    def end_position(self):
        types_list = self.__typeDealDao.get_types_deal()
        insert_tuple_1 = (types_list[0][0],)
        insert_tuple_2 = (types_list[1][0],)
        sql_insert_query_buy = """SELECT COUNT( quantity ) FROM deals WHERE type_deal_id = (%s) """
        sql_insert_query_sell = """SELECT COUNT( quantity ) FROM deals WHERE type_deal_id = (%s)"""

        number_of_buys = self.__db.select(sql_insert_query_buy, insert_tuple_1).fetchall()
        number_of_sells = self.__db.select(sql_insert_query_sell, insert_tuple_2).fetchall()
        end_position = number_of_buys[0][0] - number_of_sells[0][0]

        result = {}
        if number_of_buys > number_of_sells:
            result["end_position"] = f"You are long {end_position} position"
        elif number_of_sells == number_of_buys:
            result["end_position"] = "You are neutral"
        else:
            result["end_position"] = f"You are short {-end_position}"

        return result

    def get_profit_or_loss(self):
        types_list = self.__typeDealDao.get_types_deal()
        insert_tuple_1 = (types_list[0][0],)
        insert_tuple_2 = (types_list[1][0],)
        sql_insert_query_buy = """SELECT SUM(price) FROM deals WHERE type_deal_id = (%s) """
        sql_insert_query_sell = """SELECT SUM(price) FROM deals WHERE type_deal_id = (%s) """
        loss = self.__db.select(sql_insert_query_buy, insert_tuple_1).fetchall()
        profit = self.__db.select(sql_insert_query_sell, insert_tuple_2).fetchall()

        balance = profit[0][0] - loss[0][0]

        result = {}
        if balance > 0:
            result["profit"] =f"Made a  profit of {balance}"
        elif balance == 0:
            result["neutral"] = f"Neither made profit nor loss"
        else:
            result["loss"] = f"Made a loss of {balance}"

        return result

    def get_effective_profit_loss(self):
        types_list = self.__typeDealDao.get_types_deal()
        insert_tuple_1 = (types_list[0][0],)
        insert_tuple_2 = (types_list[1][0],)
        sql_insert_query_buy = """SELECT SUM(price * quantity) FROM deals WHERE type_deal_id = (%s)  """
        sql_insert_query_sell = """SELECT SUM(price * quantity) FROM deals WHERE type_deal_id = (%s) """

        loss = self.__db.select(sql_insert_query_buy, insert_tuple_1).fetchall()
        profit = self.__db.select(sql_insert_query_sell, insert_tuple_2).fetchall()

        balance = profit[0][0] - loss[0][0]

        result = {}
        if balance > 0 or balance < 0:
            result["effective profit/loss"] = f"Your net balance is {balance}"
        else:
            result["effective profit/loss"] = "Your net balance is 0"

        return result