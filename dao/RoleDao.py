from dbconnect.DBconnect import DBconnect


class RoleDao:
    __db = None

    def __init__(self):
        self.__db = DBconnect()

    def is_exist_role(self, role):
        sql_insert_query = """SELECT * FROM roles WHERE  role = (%s)"""
        insert_tuple = (role,)
        result = self.__db.select(sql_insert_query, insert_tuple).fetchall()
        if (len(result) == 0):
            return False
        else:
            return True

    def save_role(self, role):
        if not(self.is_exist_role(role)):
            sql_insert_query = """ INSERT INTO roles(role) VALUES (%s)"""
            insert_tuple = (role,)
            return self.__db.insert(sql_insert_query, insert_tuple)
        else:
            return False

    def get_role_id(self, role):
        if (self.is_exist_role(role)):
            sql_select_query = """ SELECT id FROM roles WHERE role=(%s)"""
            select_tuple = (role,)
            result = self.__db.select(sql_select_query, select_tuple).fetchall()
            return result[0][0]
        else:
            return -1

    def get_role(self, id):
        sql_select_query = """ SELECT role FROM roles WHERE id=(%s)"""
        select_tuple = (id,)
        result = self.__db.select(sql_select_query, select_tuple).fetchall()
        if len(result) == 0:
            return None
        return result[0][0]


