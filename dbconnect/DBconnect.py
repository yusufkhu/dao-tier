import json

import mysql.connector

class DBconnect(object):
    __connection = None
    __cursor = None

    def __init__(self):
        with open('./config/db-config.json') as f:
            config = json.load(f)
        __db_config = config['mysql']
        self.__connection = mysql.connector.connect(
                                            host = __db_config['host'],
                                            port = __db_config['port'],
                                            user = __db_config['user'],
                                            password = __db_config['password'],
                                            db = __db_config['db'])
        self.__cursor = self.__connection.cursor()

    def select(self, query, params):
       self.__cursor.execute(query, params)
       return self.__cursor

    def insert(self, query, params):
       try:
           self.__cursor.execute(query, params)
           self.__connection.commit()
           return True
       except Exception:
           return False

    def close(self):
        self.__connection.close()