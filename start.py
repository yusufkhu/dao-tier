
from flask import Flask, Response, jsonify, request
from flask_cors import CORS

from dao.CptyDao import CptyDao
from dao.DealDao import DealDao
from dao.InstrumentDao import InstrumentDao
from dao.TypeDealDao import TypeDealDao
from dao.UserDao import UserDao
from models.Deal import Deal
from models.User import User
import json

def create_app():
    '''Create an app by initializing components'''
    application = Flask(__name__)
    return application

application = create_app()
CORS(application)

#---------------------POST REQUEST------------------------

@application.route('/auth', methods=['POST'])
def auth():
    js = json.loads(request.get_json())
    user = User(js['username'], js['password'])
    if userDao.check_username_and_password_for_user(user):
        return jsonify(status=200, msg='OK', role = userDao.get_role_for_user(user))
    else:
        return jsonify(status=403, msg='User is not exist!')

@application.route('/save_user', methods=['POST'])
def save_user():
    js = json.loads(request.get_json())
    user = User(js['username'], js['password'], js['role'])
    if userDao.save_user(user):
        return jsonify(status=200, msg='OK')
    else:
        return jsonify(status=403, msg='User was not saving!')

@application.route('/save_deal', methods=['POST'])
def save_deal():
    js = request.get_json()
    deal = Deal(js['instrumentName'], js['cpty'], js['price'], js['type'], js['quantity'], js['time'])
    if dealDao.save_deal(deal):
        return jsonify(status=200, msg='OK')
    else:
        return jsonify(status=403, msg='Deal was not saving!')

#-----------------------GET REQUEST------------------------

@application.route('/get_cptys', methods=['GET'])
def get_cptys():
    list_cptys = cptyDao.get_cptys()
    if len(list_cptys) == 0:
        return jsonify(status=403, msg='Cptys are not exist!', data=None)
    list_data = []
    for cpty in list_cptys:
        dict_cp = {}
        dict_cp["id"] = cpty[0]
        dict_cp["name"] = cpty[1]
        list_data.append(dict_cp)
    return jsonify(status=200, msg='OK', data = list_data)

@application.route('/get_instruments', methods=['GET'])
def get_instruments():
    list_instruments = instrumentDao.get_instruments()
    if len(list_instruments) == 0:
        return jsonify(status= 403, msg='Instrument are not exist!', data=None)
    list_data = []
    for cpty in list_instruments:
        dict_cp = {}
        dict_cp["id"] = cpty[0]
        dict_cp["name"] = cpty[1]
        list_data.append(dict_cp)
    return jsonify(status=200, msg='OK', data = list_data)

@application.route('/get_types', methods=['GET'])
def get_types():
    list_types = typeDao.get_types_deal()
    if len(list_types) == 0:
        return jsonify(status= 403, msg='Types are not exist!', data=None)
    list_data = []
    for cpty in list_types:
        dict_cp = {}
        dict_cp["id"] = cpty[0]
        dict_cp["name"] = cpty[1]
        list_data.append(dict_cp)
    return jsonify(status=200, msg='OK', data = list_data)


pageNumber = 0
pageSize = 10
pages = 0

@application.route('/get_deals', methods=['GET'])
def get_deals():
    global pages
    global pageNumber
    global pageSize
    num = request.args.get('pageNumber')
    date_from = request.args.get('dateFrom')
    date_to = request.args.get('dateTo')
    if num is not None and num != '':
        if int(num) <= 0:
            pageNumber = 0
        else:
            pageNumber = int(num) - 1

    if (date_from is not None and date_from!='')or(date_to is not None and date_to!=''):
        if pageNumber is None or pageNumber=='':
            pageNumber = 0
        list_deals = dealDao.get_historical_date(date_from, date_to)
    else:
        list_deals = dealDao.get_deals()

    if len(list_deals) == 0:
        return jsonify(status=403, msg='Deals are not exist!', data=None)

    pages = len(list_deals)/pageSize
    if pages!=int(pages):
        pages = int(pages) + 1

    if pages <= pageNumber:
        return jsonify(status=403, msg='Deals are not exist!', data=None)
    else:
        pageNumber += 1
        deal_list = get_json_deal(pageSize, pageNumber-1, list_deals)
        return jsonify(status=200, msg='Ok', data=deal_list,
                       pageNumber=pageNumber, pageSize=len(deal_list), pages=pages)



@application.route('/avg_B_and_S_for_inst', methods=['GET'])
def get_avg_for_instrument_during_period():
    types_list, instrument_list = typeDao.get_types_deal(), instrumentDao.get_instruments()
    date_from, date_to = request.args.get('dateFrom'), request.args.get('dateTo')
    sell_list, buy_list = [], []
    result = {}
    for inst in range(len(instrument_list)):
        b, s = {}, {}
        b["instrumentName"], s["instrumentName"] = instrument_list[inst][1], instrument_list[inst][1]
        if (date_from is None or date_from == '') or (date_to is None or date_to == ''):
            b["avg_price"] = instrumentDao.average_price_without_date(types_list[0][0], instrument_list[inst][0])[0][0]
            s["avg_price"] = instrumentDao.average_price_without_date(types_list[1][0], instrument_list[inst][0])[0][0]
        else:
            b["avg_price"] = instrumentDao.average_price(types_list[0][0], instrument_list[inst][0], date_from, date_to)[0][0]
            s["avg_price"] = instrumentDao.average_price(types_list[1][0], instrument_list[inst][0], date_from, date_to)[0][0]
        buy_list.append(b)
        sell_list.append(s)
    result['B'] = buy_list
    result['S'] = sell_list
    return jsonify(status=200, msg='OK', data = result)


@application.route('/end_position', methods=['GET'])
def get_end_position():
    return jsonify(status=200, msg='OK', data = dealDao.end_position())

@application.route('/get_profit_loss', methods=['GET'])
def get_profit_loss():
    return jsonify(status=200, msg='OK', data = dealDao.get_profit_or_loss())

@application.route('/get_effective_profit_loss', methods=['GET'])
def get_effective_profit_loss():
    return jsonify(status=200, msg='OK', data = dealDao.get_effective_profit_loss())

#__________________END REQUESTS_____________________


def get_json_deal(size, number, list_deals):
    list_data = []
    count = 0
    for deal in range(number*size, number*size+10):
        if (count == size) or deal == len(list_deals):
            break
        dict_cp = {}
        dict_cp["id"] = list_deals[deal][0]
        dict_cp["instrumentName"] = list_deals[deal][1]
        dict_cp["cpty"] = list_deals[deal][2]
        dict_cp["price"] = list_deals[deal][3]
        dict_cp["type"] = list_deals[deal][4]
        dict_cp["quantity"] = list_deals[deal][5]
        dict_cp["time"] = list_deals[deal][6]
        list_data.append(dict_cp)
        count += 1
    return list_data

def dao_server_config():
    with open('./config/server-config.json') as f:
        config = json.load(f)
    return config['dao-server']

if __name__ == '__main__':
    config = dao_server_config()
    userDao = UserDao()
    dealDao = DealDao()
    cptyDao = CptyDao()
    typeDao = TypeDealDao()
    instrumentDao = InstrumentDao()
    print("_______________Running dao_______________")
    application.run(port=config['port'], threaded=True, host=(config['host']))
